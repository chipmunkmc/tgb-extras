package land.chipmunk.tgbextras;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.command.CommandRegistryAccess;
import net.minecraft.server.command.CommandManager;
import land.chipmunk.tgbextras.commands.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Extras implements ModInitializer {
  public static final Logger LOGGER = LoggerFactory.getLogger("tgb-extras");

  @Override
  public void onInitialize () {
    LOGGER.info("Extras initialized!");

    CommandRegistrationCallback.EVENT.register(Extras::registerCommands);
  }

  public static void registerCommands (CommandDispatcher<ServerCommandSource> dispatcher, CommandRegistryAccess commandRegistryAccess, CommandManager.RegistrationEnvironment environment) {
    AFKCommand.register(dispatcher);
    ClearChatCommand.register(dispatcher);
    PingCommand.register(dispatcher);
    SpawnCoordinatesCommand.register(dispatcher);
    TGBCommand.register(dispatcher);
    TPAskCommand.register(dispatcher);
    TPAcceptCommand.register(dispatcher);
    TPDenyCommand.register(dispatcher);
  }
}