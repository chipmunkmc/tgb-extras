package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.text.Text;

import static net.minecraft.server.command.CommandManager.literal;

public class SpawnCoordinatesCommand {
  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher.register(
      literal("spawncoords")
        .executes(SpawnCoordinatesCommand::spawnCoordinatesCommand)
    );
  }

  public static int spawnCoordinatesCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();

    final BlockPos spawnPoint = player.getSpawnPointPosition();

    if (spawnPoint == null) {
      source.sendFeedback(() -> Text.literal("Your spawn point is at world spawn"), false);
      return Command.SINGLE_SUCCESS;
    }

    final String spawnPointStr = String.format("%d %d %d", spawnPoint.getX(), spawnPoint.getY(), spawnPoint.getZ());
    source.sendFeedback(() -> Text.literal("Your spawn point is at ").append(spawnPointStr), false);

    return Command.SINGLE_SUCCESS;
  }
}