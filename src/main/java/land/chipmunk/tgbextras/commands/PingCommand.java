package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.argument.EntityArgumentType.player;
import static net.minecraft.command.argument.EntityArgumentType.getPlayer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.server.network.ServerPlayerEntity;

public class PingCommand {
  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("ping")
        .executes(PingCommand::pingCommand)
          .then(
            argument("target", player())
              .executes(PingCommand::targettedPingCommand)
          )
    );

    dispatcher.register(literal("delay").executes(PingCommand::pingCommand).redirect(node));
    dispatcher.register(literal("ms").executes(PingCommand::pingCommand).redirect(node));
  }

  public static int pingCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();
    final int ping = player.networkHandler.getLatency();
    final Formatting highlighting = getHighlighting(ping);

    final Text output = Text.literal("Your ping is ")
      .append(Text.literal(String.valueOf(ping)).formatted(highlighting))
      .append(Text.literal("ms").formatted(highlighting));
    source.sendFeedback(() -> output, false);

    return Command.SINGLE_SUCCESS;
  }

  public static int targettedPingCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity target = getPlayer(context, "target");
    final int ping = target.networkHandler.getLatency();
    final Formatting highlighting = getHighlighting(ping);

    final Text output = Text.literal("")
      .append(target.getDisplayName())
      .append(Text.literal("'s"))
      .append(Text.literal(" ping is "))
      .append(Text.literal(String.valueOf(ping)).formatted(highlighting))
      .append(Text.literal("ms").formatted(highlighting));
    source.sendFeedback(() -> output, false);

    return Command.SINGLE_SUCCESS;
  }

  private static Formatting getHighlighting (int ping) {
    final int d = (int) Math.floor((float) ping / 100);
    Formatting highlighting = Formatting.WHITE;

    switch (d) {
      case 0:
        highlighting = Formatting.GREEN;
        break;
        case 1:
        case 2:
        case 3:
        case 4:
        highlighting = Formatting.YELLOW;
        break;
      case 5:
        highlighting = Formatting.RED;
        break;
      default:
        if (d > 5) {
          highlighting = Formatting.DARK_RED;
        }
        break;
    }

    return highlighting;
  }
}
