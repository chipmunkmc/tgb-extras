package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.argument.EntityArgumentType.player;
import static net.minecraft.command.argument.EntityArgumentType.getPlayer;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import land.chipmunk.tgbextras.modules.PlayerExtensions;

public class TPAskCommand {
  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("tpask")
          .then(
            argument("target", player())
              .executes(TPAskCommand::tpAskCommand)
          )
    );

    dispatcher.register(literal("tpa").redirect(node));
  }

  public static int tpAskCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();
    final ServerPlayerEntity target = getPlayer(context, "target");

    requestTeleportation(player, target);
    source.sendFeedback(() -> Text.literal("Requested to teleport to ").append(target.getDisplayName()), false);

    return Command.SINGLE_SUCCESS;
  }

  private static void requestTeleportation (ServerPlayerEntity player, ServerPlayerEntity target) {
    PlayerExtensions targetEx = (PlayerExtensions) (Object) target;

    targetEx.lastTeleportRequestor(player);
    target.sendMessage(
      Text.literal("")
        .append(player.getDisplayName())
        .append(" has requested to teleport to you\nTo accept, run /tpaccept\nTo deny, run /tpdeny")
    );
  }
}
