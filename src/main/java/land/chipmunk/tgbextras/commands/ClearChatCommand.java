package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.tree.LiteralCommandNode;
import static net.minecraft.server.command.CommandManager.literal;
import static net.minecraft.server.command.CommandManager.argument;
import static net.minecraft.command.argument.EntityArgumentType.players;
import static net.minecraft.command.argument.EntityArgumentType.getPlayers;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.server.network.ServerPlayerEntity;
import java.util.Collection;
import java.util.List;

public class ClearChatCommand {
  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    final LiteralCommandNode node = dispatcher.register(
      literal("clearchat")
        .requires(ClearChatCommand::requirement)
        .executes(ClearChatCommand::clearChatCommand)
          .then(
            argument("targets", players())
              .executes(ClearChatCommand::targettedClearChatCommand)
          )
    );

    dispatcher.register(literal("cc").requires(ClearChatCommand::requirement).executes(ClearChatCommand::clearChatCommand).redirect(node));
  }

  public static boolean requirement (ServerCommandSource source) {
    return source.hasPermissionLevel(2);
  }

  public static int clearChatCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final List<ServerPlayerEntity> players = source.getServer().getPlayerManager().getPlayerList();

    for (final ServerPlayerEntity player : players) clearChat(player);

    source.sendFeedback(() -> Text.literal("Cleared the chat for all players"), false);

    return Command.SINGLE_SUCCESS;
  }

  public static int targettedClearChatCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final Collection<ServerPlayerEntity> players = getPlayers(context, "targets");

    for (final ServerPlayerEntity player : players) clearChat(player);

    if (players.size() == 1) {
      source.sendFeedback(() -> Text.literal("Cleared the chat for ").append(players.iterator().next().getDisplayName()), false);
    } else {
      source.sendFeedback(() -> Text.literal("Cleared the chat for ")
        .append(Text.literal(Integer.toString(players.size())))
        .append(Text.literal(" players")), false);
    }

    return Command.SINGLE_SUCCESS;
  }

  private static void clearChat (ServerPlayerEntity player) {
    final Text text = Text.literal("\n".repeat(100) + "The chat has been cleared").formatted(Formatting.DARK_GREEN);
    player.sendMessage(text);
  }
}
