package land.chipmunk.tgbextras.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import static net.minecraft.server.command.CommandManager.literal;
import land.chipmunk.tgbextras.modules.PlayerExtensions;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;

public class AFKCommand {
  public static void register (CommandDispatcher<ServerCommandSource> dispatcher) {
    dispatcher.register(
      literal("afk")
        .executes(AFKCommand::AFKCommand)
    );
  }

  public static int AFKCommand (CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
    final ServerCommandSource source = context.getSource();
    final ServerPlayerEntity player = source.getPlayerOrThrow();
    PlayerExtensions playerEx = (PlayerExtensions) player;

    playerEx.afk(!playerEx.afk());

    if (playerEx.afk()) {
      player.addStatusEffect(new StatusEffectInstance(StatusEffects.RESISTANCE, 2147483647, 4, true, false));
      player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 2147483647, 99, true, false));
      player.addStatusEffect(new StatusEffectInstance(StatusEffects.SLOWNESS, 2147483647, 99, true, false));
      player.addStatusEffect(new StatusEffectInstance(StatusEffects.WEAKNESS, 2147483647, 99, true, false));
      player.addStatusEffect(new StatusEffectInstance(StatusEffects.MINING_FATIGUE, 2147483647, 99, true, false));

      source.getServer().getPlayerManager().broadcast(
        Text.literal("* ")
          .append(player.getDisplayName())
          .append(Text.literal(" is now afk"))
      , false);
    } else {
      player.clearStatusEffects();

      source.getServer().getPlayerManager().broadcast(
        Text.literal("* ")
          .append(player.getDisplayName())
          .append(Text.literal(" is no longer afk"))
      , false);
    }

    return Command.SINGLE_SUCCESS;
  }
}