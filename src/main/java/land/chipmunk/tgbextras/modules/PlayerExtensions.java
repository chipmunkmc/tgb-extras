package land.chipmunk.tgbextras.modules;

import net.minecraft.server.network.ServerPlayerEntity;

public interface PlayerExtensions {
  ServerPlayerEntity lastTeleportRequestor ();
  ServerPlayerEntity lastTeleportRequestor (ServerPlayerEntity lastTeleportRequestor);
  boolean afk ();
  boolean afk (boolean afk);
}
