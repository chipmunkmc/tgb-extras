package land.chipmunk.tgbextras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.network.ServerPlayerEntity;
import land.chipmunk.tgbextras.modules.PlayerExtensions;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPlayerEntityMixin implements PlayerExtensions {
  private ServerPlayerEntity lastTeleportRequestor = null;
  private boolean afk = false;

  public ServerPlayerEntity lastTeleportRequestor () {
    return lastTeleportRequestor;
  }

  public ServerPlayerEntity lastTeleportRequestor (ServerPlayerEntity lastTeleportRequestor) {
    return (this.lastTeleportRequestor = lastTeleportRequestor);
  }

  public boolean afk () {
    return afk;
  }

  public boolean afk (boolean afk) {
    return (this.afk = afk);
  }
}