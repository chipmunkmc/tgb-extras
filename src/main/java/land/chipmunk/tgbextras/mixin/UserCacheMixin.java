package land.chipmunk.tgbextras.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import java.util.Optional;

@Mixin(net.minecraft.util.UserCache.class)
public abstract class UserCacheMixin {
  @Shadow private static Optional<GameProfile> getOfflinePlayerProfile (String name) { throw new AssertionError(); }

  @Inject(at = @At("HEAD"), method = "findProfileByName", cancellable = true)
  private static void findProfileByName (GameProfileRepository repository, String name, CallbackInfoReturnable<Optional<GameProfile>> info) {
    final Optional<GameProfile> optional = getOfflinePlayerProfile(name);
    info.setReturnValue(optional);
  }
}